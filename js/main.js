//Теория
//Я понимаю обработчик событий, как функцию, которая начинает выполняться, как только произошло то, или иное событие.

//Задание

const input = document.createElement('input');
const label = document.createElement('label');

const setGreenColor = function() {
    this.style.outlineColor = 'green';
}

const getNewSpan = function() {
    if(isNaN(this.value) || this.value === '') {
        return this.value = '';
    } else if (this.value < 0) {
        this.style.outline = '2px solid red';
        const errMess = document.createElement('div');
        errMess.textContent = 'Please enter correct price';
        errMess.className = 'errMess-style';
        this.addEventListener('focus', ()=>{
            errMess.remove();
        })
        return label.append(errMess);
    }
    const span = document.createElement('span');
    const btn = document.createElement('button');
    function removeSpan() {
        span.remove();
        input.value = '';
    }
    span.textContent = `Текущая цена: ${this.value}`;
    span.className = 'span-style';
    btn.className = 'btn-style';
    btn.textContent = 'X';
    btn.addEventListener('click', removeSpan);
    span.append(btn);
    document.body.prepend(span);
}

label.textContent = 'Ціна, $ ';
label.append(input);
label.className = 'label-style';
input.className = 'input-style';
document.body.append(label);

input.addEventListener('focus', setGreenColor);
input.addEventListener('blur', getNewSpan);
